package com.kolibru.hackworld.Adapters;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kolibru.hackworld.Item.NewsItem;
import com.kolibru.hackworld.MyApp;
import com.kolibru.hackworld.R;
import com.kolibru.hackworld.databinding.ListItemMainBinding;

import java.util.List;

public class NewsAdapter extends BaseAdapter<NewsItem, NewsAdapter.CardViewHolder> {


    public NewsAdapter(List<NewsItem> newsItems) {
        super(newsItems);
    }

    @Override
    public View onCreateView(ViewGroup parent, int viewType) {
        return LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_main, parent, false);
    }

    @Override
    public CardViewHolder onCreateViewHolder(View itemView) {
        return new CardViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CardViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);
        NewsItem newsItem = source.get(position);
        holder.itemView.setTag(R.id.position, position);
        holder.listItemMainBinding.setNews(newsItem);//вот так легко заполнится xml файл всеми полями из объекта
        try {
            if (newsItem.img.get() != null)
                holder.listItemMainBinding.newsPic.setImageResource(newsItem.img.get());
        }
        catch (Exception e){

        }
    }


    static class CardViewHolder extends BaseAdapter.ViewHolder {
        ListItemMainBinding listItemMainBinding;//Дата биндинг нашего xml файла list_item_main, этот класс содается автоматически когда прописываем в файле структуру layout и подключение каких либо классов

        public CardViewHolder(View itemView) {
            super(itemView);
            listItemMainBinding = DataBindingUtil.bind(itemView);
        }
    }


}
