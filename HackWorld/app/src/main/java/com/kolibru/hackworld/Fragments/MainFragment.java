package com.kolibru.hackworld.Fragments;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.florent37.materialviewpager.MaterialViewPagerHelper;
import com.github.florent37.materialviewpager.adapter.RecyclerViewMaterialAdapter;
import com.kolibru.hackworld.Item.NewsItem;
import com.kolibru.hackworld.ModelViews.NewsViewModel;
import com.kolibru.hackworld.R;
import com.kolibru.hackworld.databinding.FragmentMainBinding;

import java.util.ArrayList;
import java.util.List;


public class MainFragment extends Fragment {

    List<NewsItem> data = new ArrayList<>();
    private NewsViewModel NVM;
    private FragmentMainBinding mBinding;

    private static final String FILTER_TIMER = "filter_timer";


    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View fragment_view = inflater.inflate(R.layout.fragment_main, container, false);
        return fragment_view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        data = getDatalist();

        NVM = new NewsViewModel();//создаем модель работы с содержимым RecycleView
        for (NewsItem ni : data)
            NVM.addNews(ni);
        mBinding = DataBindingUtil.bind(getView());//получаем структуру XML фрагмента - куда будет помещена модель

        mBinding.mainList.setLayoutManager(new LinearLayoutManager(getActivity()));
        mBinding.mainList.setAdapter(new RecyclerViewMaterialAdapter(NVM.NA));
//        NVM.setOnNewsClickListener(new NewsViewModel.OnNewsClickListener() {//задаем обработчик нажатия
//            @Override
//            public void onNewsClick(NewsItem mNews) {
//                Intent intent = new Intent(getActivity(), NewsDetailActivity.class);
//                intent.putExtra(NewsItem.class.getCanonicalName(), mNews);
//                startActivity(intent);
//            }
//        });
        mBinding.setNewsViewModel(NVM);//помещаем модель
        MaterialViewPagerHelper.registerRecyclerView(getActivity(), mBinding.mainList, null);

    }


    private List<NewsItem> getDatalist() {
        List<NewsItem> data = new ArrayList<>();
        data.add(new NewsItem("Регистрация", "", "18:00-19:00").withImage(R.drawable.ic_user).withSubhead("В холле 7 корпуса Вас ждут наши волонтеры, которые помогут во всех Ваших желаниях"));
        data.add(new NewsItem("Официальное открытие", "", "19:00-20:00").withImage(R.drawable.ic_light).withSubhead("Вас ждет увлекательная программа, спонсоры и официальные лица мероприятия, а так же самый увлекательный Хакатон"));
        data.add(new NewsItem("Слово экспертам", "", "20:00-21:00").withImage(R.drawable.ic_speak));
        data.add(new NewsItem("Презентация кейсов", "", " 21:00-22:00").withImage(R.drawable.ic_profile).withSubhead("Каждый участник мероприятия кратко рассказывает о своих достижениях, о том, что он ждет от мероприятия и какую идею он хочет реализовать"));
        data.add(new NewsItem("Формирование команд", "", "21:00-23:00").withImage(R.drawable.ic_people));
        data.add(new NewsItem("Практическая работа над проектом , чекпоинт для руководителей проектов ", "", "22:00-9:00").withImage(R.drawable.ic_code).withSubhead("Начинаем кодить"));
        data.add(new NewsItem("Кофе брейк ", "", "02:00-2:30").withImage(R.drawable.ic_coffe).withSubhead("Небольшой перерывчик, для приема бодрящего кофе"));
        return data;
    }

}
