package com.kolibru.hackworld.Fragments;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.kolibru.hackworld.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

@EFragment(R.layout.fragment_map)
public class MapFragment extends Fragment {

    @ViewById
    ImageButton toilet,toilet2,code,code2,code3,food,coffe,coffe2,out,out2,out3,contract,lection;

    @ViewById
    FloatingActionButton fab2;


    @AfterViews
    void ready() {
        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(getView(), "Сбросить фильтр", Snackbar.LENGTH_LONG)
                        .setAction(R.string.show, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                coffe2.setVisibility(View.VISIBLE);
                                food.setVisibility(View.VISIBLE);
                                contract.setVisibility(View.VISIBLE);
                                out2.setVisibility(View.VISIBLE);
                                out3.setVisibility(View.VISIBLE);
                                toilet.setVisibility(View.VISIBLE);
                                toilet2.setVisibility(View.VISIBLE);
                                code.setVisibility(View.VISIBLE);
                                code2.setVisibility(View.VISIBLE);
                                code3.setVisibility(View.VISIBLE);
                                coffe.setVisibility(View.VISIBLE);
                                lection.setVisibility(View.VISIBLE);
                                out.setVisibility(View.VISIBLE);
                            }
                        })
                        .show();
            }
        });
        toilet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(v, "Показать  уборные", Snackbar.LENGTH_LONG)
                        .setAction(R.string.show, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                food.setVisibility(View.GONE);
                                out2.setVisibility(View.GONE);
                                out3.setVisibility(View.GONE);
                                contract.setVisibility(View.GONE);
                                coffe2.setVisibility(View.GONE);
                                code.setVisibility(View.GONE);
                                code2.setVisibility(View.GONE);
                                code3.setVisibility(View.GONE);
                                coffe.setVisibility(View.GONE);
                                lection.setVisibility(View.GONE);
                                out.setVisibility(View.GONE);
                            }
                        })
                        .show();
            }
        });
        toilet2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(v, "Показать  уборные", Snackbar.LENGTH_LONG)
                        .setAction(R.string.show, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                food.setVisibility(View.GONE);
                                out2.setVisibility(View.GONE);
                                out3.setVisibility(View.GONE);
                                contract.setVisibility(View.GONE);
                                coffe2.setVisibility(View.GONE);
                                code.setVisibility(View.GONE);
                                code2.setVisibility(View.GONE);
                                code3.setVisibility(View.GONE);
                                coffe.setVisibility(View.GONE);
                                lection.setVisibility(View.GONE);
                                out.setVisibility(View.GONE);
                            }
                        })
                        .show();
            }
        });
        code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(v, "Показать рабочую зону", Snackbar.LENGTH_LONG)
                        .setAction(R.string.show, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                toilet.setVisibility(View.GONE);
                                toilet2.setVisibility(View.GONE);
                                food.setVisibility(View.GONE);
                                out2.setVisibility(View.GONE);
                                out3.setVisibility(View.GONE);
                                contract.setVisibility(View.GONE);
                                coffe2.setVisibility(View.GONE);
                                coffe.setVisibility(View.GONE);
                                lection.setVisibility(View.GONE);
                                out.setVisibility(View.GONE);
                            }
                        })
                        .show();
            }
        });
        code2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(v, "Показать рабочую зону", Snackbar.LENGTH_LONG)
                        .setAction(R.string.show, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                toilet.setVisibility(View.GONE);
                                toilet2.setVisibility(View.GONE);
                                food.setVisibility(View.GONE);
                                out2.setVisibility(View.GONE);
                                out3.setVisibility(View.GONE);
                                contract.setVisibility(View.GONE);
                                coffe2.setVisibility(View.GONE);
                                coffe.setVisibility(View.GONE);
                                lection.setVisibility(View.GONE);
                                out.setVisibility(View.GONE);
                            }
                        })
                        .show();
            }
        });
        code3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(v, "Показать рабочую зону", Snackbar.LENGTH_LONG)
                        .setAction(R.string.show, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                toilet.setVisibility(View.GONE);
                                toilet2.setVisibility(View.GONE);
                                food.setVisibility(View.GONE);
                                out2.setVisibility(View.GONE);
                                out3.setVisibility(View.GONE);
                                contract.setVisibility(View.GONE);
                                coffe2.setVisibility(View.GONE);
                                coffe.setVisibility(View.GONE);
                                lection.setVisibility(View.GONE);
                                out.setVisibility(View.GONE);
                            }
                        })
                        .show();
            }
        });
        coffe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(v, "Показать кофе брейк зону", Snackbar.LENGTH_LONG)
                        .setAction(R.string.show, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                toilet.setVisibility(View.GONE);
                                toilet2.setVisibility(View.GONE);
                                code.setVisibility(View.GONE);
                                code2.setVisibility(View.GONE);
                                code3.setVisibility(View.GONE);
                                lection.setVisibility(View.GONE);
                                out.setVisibility(View.GONE);
                                food.setVisibility(View.GONE);
                                out2.setVisibility(View.GONE);
                                out3.setVisibility(View.GONE);
                                contract.setVisibility(View.GONE);
                            }
                        })
                        .show();
            }
        });
        lection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(v, "Показать зону лекций", Snackbar.LENGTH_LONG)
                        .setAction(R.string.show, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                toilet.setVisibility(View.GONE);
                                toilet2.setVisibility(View.GONE);
                                code.setVisibility(View.GONE);
                                code2.setVisibility(View.GONE);
                                code3.setVisibility(View.GONE);
                                coffe.setVisibility(View.GONE);
                                out.setVisibility(View.GONE);
                                food.setVisibility(View.GONE);
                                out2.setVisibility(View.GONE);
                                out3.setVisibility(View.GONE);
                                contract.setVisibility(View.GONE);
                                coffe2.setVisibility(View.GONE);

                            }
                        })
                        .show();
            }
        });
        out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(v, "Показать вход/выход", Snackbar.LENGTH_LONG)
                        .setAction(R.string.show, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                toilet.setVisibility(View.GONE);
                                toilet2.setVisibility(View.GONE);
                                code.setVisibility(View.GONE);
                                code2.setVisibility(View.GONE);
                                code3.setVisibility(View.GONE);
                                lection.setVisibility(View.GONE);
                                coffe.setVisibility(View.GONE);
                                food.setVisibility(View.GONE);
                                contract.setVisibility(View.GONE);
                                coffe2.setVisibility(View.GONE);


                            }
                        })
                        .show();
            }
        });
        out2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(v, "Показать вход/выход", Snackbar.LENGTH_LONG)
                        .setAction(R.string.show, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                toilet.setVisibility(View.GONE);
                                toilet2.setVisibility(View.GONE);
                                code.setVisibility(View.GONE);
                                code2.setVisibility(View.GONE);
                                code3.setVisibility(View.GONE);
                                lection.setVisibility(View.GONE);
                                coffe.setVisibility(View.GONE);
                                food.setVisibility(View.GONE);
                                contract.setVisibility(View.GONE);
                                coffe2.setVisibility(View.GONE);
                            }
                        })
                        .show();
            }
        });

        out3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(v, "Показать вход/выход", Snackbar.LENGTH_LONG)
                        .setAction(R.string.show, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                toilet.setVisibility(View.GONE);
                                toilet2.setVisibility(View.GONE);
                                code.setVisibility(View.GONE);
                                code2.setVisibility(View.GONE);
                                code3.setVisibility(View.GONE);
                                lection.setVisibility(View.GONE);
                                coffe.setVisibility(View.GONE);
                                food.setVisibility(View.GONE);
                                contract.setVisibility(View.GONE);
                                coffe2.setVisibility(View.GONE);
                            }
                        })
                        .show();
            }
        });
        contract.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(v, "Показать зону", Snackbar.LENGTH_LONG)
                        .setAction(R.string.show, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                toilet.setVisibility(View.GONE);
                                toilet2.setVisibility(View.GONE);
                                code.setVisibility(View.GONE);
                                code2.setVisibility(View.GONE);
                                code3.setVisibility(View.GONE);
                                lection.setVisibility(View.GONE);
                                coffe.setVisibility(View.GONE);
                                food.setVisibility(View.GONE);
                                out.setVisibility(View.GONE);
                                out2.setVisibility(View.GONE);
                                out3.setVisibility(View.GONE);
                                coffe2.setVisibility(View.GONE);


                            }
                        })
                        .show();
            }
        });
        coffe2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(v, "Показать кофе брейк зону", Snackbar.LENGTH_LONG)
                        .setAction(R.string.show, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                toilet.setVisibility(View.GONE);
                                toilet2.setVisibility(View.GONE);
                                code.setVisibility(View.GONE);
                                code2.setVisibility(View.GONE);
                                code3.setVisibility(View.GONE);
                                lection.setVisibility(View.GONE);
                                out.setVisibility(View.GONE);
                                food.setVisibility(View.GONE);
                                out2.setVisibility(View.GONE);
                                out3.setVisibility(View.GONE);
                                contract.setVisibility(View.GONE);
                            }
                        })
                        .show();
            }
        });
        food.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(v, "Показать зону еды", Snackbar.LENGTH_LONG)
                        .setAction(R.string.show, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                toilet.setVisibility(View.GONE);
                                toilet2.setVisibility(View.GONE);
                                code.setVisibility(View.GONE);
                                code2.setVisibility(View.GONE);
                                code3.setVisibility(View.GONE);
                                lection.setVisibility(View.GONE);
                                out.setVisibility(View.GONE);
                                coffe.setVisibility(View.GONE);
                                coffe2.setVisibility(View.GONE);
                                out2.setVisibility(View.GONE);
                                out3.setVisibility(View.GONE);
                                contract.setVisibility(View.GONE);
                            }
                        })
                        .show();
            }
        });
    }
}
