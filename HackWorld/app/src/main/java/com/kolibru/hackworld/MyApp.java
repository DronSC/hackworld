package com.kolibru.hackworld;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.ndk.CrashlyticsNdk;

import org.joda.time.DateTimeZone;

import java.util.TimeZone;

import io.fabric.sdk.android.Fabric;


/**
 * Created by Andrej on 23.11.2015.
 */
public class MyApp extends Application {

    public static final String APP_PREFERENCES = "HackWorls";

    public static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext=getApplicationContext();
        Fabric.with(this, new Crashlytics(), new CrashlyticsNdk());//необходимо для запуска сервиса от Fabric по отлавливанию ошибок, они будут приходить на их сайт или мне на e-mail.

        DateTimeZone.setDefault(DateTimeZone.forOffsetMillis(TimeZone.getDefault().getRawOffset()));

    }

    /**
     * Активен интернет?
     *
     * @param c - Активити
     * @return
     */
    public static boolean isInternetOn(Context c) {
        ConnectivityManager connec = (ConnectivityManager) c.getApplicationContext()
                .getSystemService(c.getApplicationContext().CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connec.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }
}
