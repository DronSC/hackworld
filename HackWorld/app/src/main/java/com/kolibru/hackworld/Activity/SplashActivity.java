package com.kolibru.hackworld.Activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.kolibru.hackworld.R;
import com.kolibru.hackworld.databinding.ActivitySplashV2Binding;
import com.nineoldandroids.animation.Animator;

public class SplashActivity extends AppCompatActivity {

    ActivitySplashV2Binding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_v2);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_splash_v2);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                YoYo.with(Techniques.RollOut).duration(500).withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        YoYo.with(Techniques.RollIn).duration(500).playOn(mBinding.image);
                        mBinding.image.setImageResource(R.drawable.logo_rounder);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                }).playOn(mBinding.image);


                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startActivity(new Intent(SplashActivity.this, MainActivity_.class));
                        SplashActivity.this.finish();
                    }
                }, 2000);
            }
        }, 1000);
    }
}
