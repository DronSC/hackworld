package com.kolibru.hackworld.Item;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.ObservableField;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Andrej_Maligin on 29.11.2015.
 */
public class OrgItem extends BaseObservable implements Parcelable {

    @Bindable
    public final ObservableField<String> title = new ObservableField<>();//навзание мероприятия
    @Bindable
    public final ObservableField<String> subhead = new ObservableField<>();//описание
    @Bindable
    public final ObservableField<Integer> img = new ObservableField<>();//id картинки


    public OrgItem(String title) {
        this.title.set(title);
    }

    public OrgItem withImage(int img) {
        this.img.set(img);
        return this;
    }

    public OrgItem withSubhead(String sub) {
        this.subhead.set(sub);
        return this;
    }

    //==== для того, чтобы передавать объект целиком для любого экрана - парсим его. например нужно будет для перехода на экран детальной информации ===============
    protected OrgItem(Parcel in) {
        title.set(in.readString());
    }

    public static final Creator<OrgItem> CREATOR = new Creator<OrgItem>() {
        @Override
        public OrgItem createFromParcel(Parcel in) {
            return new OrgItem(in);
        }

        @Override
        public OrgItem[] newArray(int size) {
            return new OrgItem[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title.get());
    }
}
