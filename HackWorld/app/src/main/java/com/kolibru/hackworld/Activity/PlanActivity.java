package com.kolibru.hackworld.Activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.florent37.materialviewpager.MaterialViewPager;
import com.github.florent37.materialviewpager.MaterialViewPagerHelper;
import com.github.florent37.materialviewpager.header.HeaderDesign;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.kolibru.hackworld.Fragments.MainFragment;
import com.kolibru.hackworld.Fragments.SecondFragment;
import com.kolibru.hackworld.Fragments.ThirdFragment;
import com.kolibru.hackworld.R;
import com.mikepenz.materialdrawer.Drawer;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_plan)
public class PlanActivity extends AppCompatActivity {

    @ViewById(R.id.mViewPagerContent)
    MaterialViewPager mViewPager;

    @ViewById(R.id.drawer_layout)
    DrawerLayout mDrawer;

    private ActionBarDrawerToggle mDrawerToggle;
    private Toolbar toolbar;


    @AfterViews
    void ready() {
        setTitle("");
        toolbar = mViewPager.getToolbar();
        if (toolbar != null) {
            setSupportActionBar(toolbar);

            ActionBar actionBar = getSupportActionBar();
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setDisplayUseLogoEnabled(false);
            actionBar.setHomeButtonEnabled(true);
        }


        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawer, 0, 0);
        mDrawer.setDrawerListener(mDrawerToggle);

        mViewPager.getViewPager().setAdapter(new MyPagerAdapter(getSupportFragmentManager()));
        mViewPager.getViewPager().setOffscreenPageLimit(mViewPager.getViewPager().getAdapter().getCount());
        mViewPager.getPagerTitleStrip().setViewPager(mViewPager.getViewPager());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mViewPager.setImageDrawable(getDrawable(R.drawable.header),400);
        } else {
            mViewPager.setImageDrawable(getResources().getDrawable(R.drawable.header),400);
        }

    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {

            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    class MyPagerAdapter extends FragmentStatePagerAdapter {

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return (new MainFragment());
                case 1:
                    return (new SecondFragment());
                case 2:
                    return (new ThirdFragment());
                default:
                    return null;
            }
        }


        @Override
        public CharSequence getPageTitle(int position) {
            switch (position % 3) {
                case 0:
                    return "Первый день";
                case 1:
                    return "Второй день";
                case 2:
                    return "Третий день";
            }
            return "";
        }


        @Override
        public int getCount() {
            return 3;
        }
    }


}
