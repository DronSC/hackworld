package com.kolibru.hackworld.ModelViews;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.ObservableArrayList;

import com.kolibru.hackworld.Adapters.NewsAdapter;
import com.kolibru.hackworld.Item.NewsItem;


public class NewsViewModel extends BaseObservable {
    public final NewsAdapter NA;
    OnNewsClickListener mOnNewsClickListener = null;

    @Bindable
    public ObservableArrayList<NewsItem> items = new ObservableArrayList<>();


    public NewsViewModel() {
        NA = new NewsAdapter(items);
        NA.setOnItemClickListener(new NewsAdapter.OnItemClickListener<NewsItem>() {
            @Override
            public void onItemClick(NewsItem item, int position) {
                if (mOnNewsClickListener != null)
                    mOnNewsClickListener.onNewsClick(item);
            }
        });
        reloadNewss();
    }

    public void reloadNewss() {//обновление новостей из хранимого в классе списка
        items.clear();
        items.addAll(items);
        NA.notifyDataSetChanged();
    }

    public void addNews(NewsItem nNews)//добавление новости в список
    {
        items.add(nNews);
        NA.notifyItemInserted(items.indexOf(nNews));
    }

    public interface OnNewsClickListener {
        void onNewsClick(NewsItem mNews);
    }

    public void setOnNewsClickListener(OnNewsClickListener onNewsClickListener) {
        this.mOnNewsClickListener = onNewsClickListener;
    }
}
