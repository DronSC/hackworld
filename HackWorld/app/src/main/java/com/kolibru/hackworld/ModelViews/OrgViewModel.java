package com.kolibru.hackworld.ModelViews;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.ObservableArrayList;

import com.kolibru.hackworld.Adapters.NewsAdapter;
import com.kolibru.hackworld.Adapters.OrgAdapter;
import com.kolibru.hackworld.Item.NewsItem;
import com.kolibru.hackworld.Item.OrgItem;


public class OrgViewModel extends BaseObservable {
    public final OrgAdapter NA;
    OnNewsClickListener mOnNewsClickListener = null;

    @Bindable
    public ObservableArrayList<OrgItem> items = new ObservableArrayList<>();


    public OrgViewModel() {
        NA = new OrgAdapter(items);
        NA.setOnItemClickListener(new NewsAdapter.OnItemClickListener<OrgItem>() {
            @Override
            public void onItemClick(OrgItem item, int position) {
                if (mOnNewsClickListener != null)
                    mOnNewsClickListener.onNewsClick(item);
            }
        });
        reloadNewss();
    }

    public void reloadNewss() {//обновление новостей из хранимого в классе списка
        items.clear();
        items.addAll(items);
        NA.notifyDataSetChanged();
    }

    public void addNews(OrgItem nNews)//добавление новости в список
    {
        items.add(nNews);
        NA.notifyItemInserted(items.indexOf(nNews));
    }

    public interface OnNewsClickListener {
        void onNewsClick(OrgItem mNews);
    }

    public void setOnNewsClickListener(OnNewsClickListener onNewsClickListener) {
        this.mOnNewsClickListener = onNewsClickListener;
    }
}
