package com.kolibru.hackworld.Adapters;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kolibru.hackworld.Item.OrgItem;
import com.kolibru.hackworld.R;
import com.kolibru.hackworld.databinding.ListOrgItemMainBinding;
import com.kolibru.hackworld.databinding.ListSponItemMainBinding;

import java.util.List;

public class SponAdapter extends BaseAdapter<OrgItem, SponAdapter.CardViewHolder> {


    public SponAdapter(List<OrgItem> newsItems) {
        super(newsItems);
    }

    @Override
    public View onCreateView(ViewGroup parent, int viewType) {
        return LayoutInflater.from(parent.getContext()).inflate(R.layout.list_spon_item_main, parent, false);
    }

    @Override
    public CardViewHolder onCreateViewHolder(View itemView) {
        return new CardViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CardViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);
        OrgItem newsItem = source.get(position);
        holder.itemView.setTag(R.id.position, position);
        holder.listItemMainBinding.setNews(newsItem);//вот так легко заполнится xml файл всеми полями из объекта
        try {
            if (newsItem.img.get() != null)
                holder.listItemMainBinding.newsPic.setImageResource(newsItem.img.get());
        }
        catch (Exception e){

        }
    }


    static class CardViewHolder extends BaseAdapter.ViewHolder {
        ListSponItemMainBinding listItemMainBinding;//Дата биндинг нашего xml файла list_item_main, этот класс содается автоматически когда прописываем в файле структуру layout и подключение каких либо классов

        public CardViewHolder(View itemView) {
            super(itemView);
            listItemMainBinding = DataBindingUtil.bind(itemView);
        }
    }


}
