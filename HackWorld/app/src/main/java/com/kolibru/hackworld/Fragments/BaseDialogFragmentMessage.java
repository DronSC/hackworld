package com.kolibru.hackworld.Fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.github.florent37.materialviewpager.MaterialViewPagerHelper;
import com.github.florent37.materialviewpager.adapter.RecyclerViewMaterialAdapter;
import com.kolibru.hackworld.Feedback.MailSenderClass;
import com.kolibru.hackworld.Item.NewsItem;
import com.kolibru.hackworld.ModelViews.NewsViewModel;
import com.kolibru.hackworld.R;
import com.kolibru.hackworld.databinding.FragmentMainBinding;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ashotik05 on 11.12.2015.
 */
public class BaseDialogFragmentMessage extends DialogFragment {

    protected String WhoEmail="Timur.Leroy@yandex.ru";

    protected TextInputLayout title1, name1, text1;

    protected String title;
    protected String from;
    protected String where;
    protected String attach;
    protected String text;
    protected String name;
    protected String all_text;



    protected class sender_mail_async extends AsyncTask<Object, String, Boolean> {
        ProgressDialog WaitingDialog;

        @Override
        protected void onPreExecute() {
                WaitingDialog = ProgressDialog.show(getActivity(), "Выполняется", "Пожалуйста подождите", true);
        }

        @Override
        protected void onPostExecute(Boolean result) {
            WaitingDialog.dismiss();
            Toast.makeText(getActivity(), "Ваши пожелания учтены,спасибо!", Toast.LENGTH_LONG).show();
            BaseDialogFragmentMessage.this.dismiss();
        }


        @Override
        protected Boolean doInBackground(Object... params) {

            try{
                from = "feedbackmail@gmail.ru";

                MailSenderClass sender = new MailSenderClass("Feedmailback@gmail.com", "654321ytrewq");

                sender.sendMail(title, all_text, from, WhoEmail, attach);
            } catch (Exception e) {
                Toast.makeText(getActivity(), "упс!", Toast.LENGTH_SHORT).show();
            }

            return false;
        }

    }

}

