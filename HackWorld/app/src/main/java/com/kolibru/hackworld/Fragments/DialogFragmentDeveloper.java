package com.kolibru.hackworld.Fragments;

import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;

import com.github.florent37.materialviewpager.MaterialViewPagerHelper;
import com.github.florent37.materialviewpager.adapter.RecyclerViewMaterialAdapter;
import com.kolibru.hackworld.Item.NewsItem;
import com.kolibru.hackworld.ModelViews.NewsViewModel;
import com.kolibru.hackworld.R;
import com.kolibru.hackworld.databinding.FragmentMainBinding;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ashotik05 on 11.12.2015.
 */
public class DialogFragmentDeveloper extends BaseDialogFragmentMessage {



        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                                 @Nullable Bundle savedInstanceState) {
            WhoEmail="Timur.Leroy@yandex.ru";
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            View fragment_view = inflater.inflate(R.layout.send, container, false);
            fragment_view.findViewById(R.id.fab).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sender_mail_async async_sending = new sender_mail_async();
                    async_sending.execute();
                }
            });
            ((TextView) fragment_view.findViewById(R.id.title)).setText("Письмо разработчкику:");
            title1 = ((TextInputLayout) fragment_view.findViewById(R.id.txt1));
            title = ((EditText) title1.findViewById(R.id.first_name)).getText().toString();
            name1 = ((TextInputLayout) fragment_view.findViewById(R.id.txt2));
            name = ((EditText) name1.findViewById(R.id.name)).getText().toString();
            text1 = ((TextInputLayout) fragment_view.findViewById(R.id.txt3));
            text = ((EditText) text1.findViewById(R.id.about)).getText().toString();
            all_text = ("Контактные данные: " + name + "\n" + "Отзыв|Предложение: " + text);

            return fragment_view;
        }


    }

