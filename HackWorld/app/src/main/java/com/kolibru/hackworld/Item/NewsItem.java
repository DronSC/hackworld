package com.kolibru.hackworld.Item;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.ObservableField;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Andrej_Maligin on 29.11.2015.
 */
public class NewsItem extends BaseObservable implements Parcelable {

    @Bindable
    public final ObservableField<String> title = new ObservableField<>();//навзание мероприятия
    @Bindable
    public final ObservableField<String> subhead = new ObservableField<>();//описание
    @Bindable
    public final ObservableField<String> place = new ObservableField<>();
    @Bindable
    public final ObservableField<String> time = new ObservableField<>();
    @Bindable
    public final ObservableField<Integer> img = new ObservableField<>();//id картинки


    public NewsItem(String title, String place, String time) {
        this.title.set(title);
        this.place.set(place);
        this.time.set(time);
    }

    public NewsItem withImage(int img) {
        this.img.set(img);
        return this;
    }

    public NewsItem withSubhead(String sub) {
        this.subhead.set(sub);
        return this;
    }

    //==== для того, чтобы передавать объект целиком для любого экрана - парсим его. например нужно будет для перехода на экран детальной информации ===============
    protected NewsItem(Parcel in) {
        title.set(in.readString());
        place.set(in.readString());
        time.set(in.readString());
    }

    public static final Creator<NewsItem> CREATOR = new Creator<NewsItem>() {
        @Override
        public NewsItem createFromParcel(Parcel in) {
            return new NewsItem(in);
        }

        @Override
        public NewsItem[] newArray(int size) {
            return new NewsItem[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title.get());
        dest.writeString(place.get());
        dest.writeString(time.get());
    }
}
