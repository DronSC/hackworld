package com.kolibru.hackworld.Feedback;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.Toast;

import com.kolibru.hackworld.R;

public class ExtendedMailOrg extends Activity {


    Context mainContext;

    TextInputLayout title1, name1, text1;

    String title;
    String from;
    String where;
    String attach;
    String text;
    private String name;
    private String all_text;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.send);

        mainContext = this;
        attach = "";

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        ((FloatingActionButton) findViewById(R.id.fab)).setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                sender_mail_async async_sending = new sender_mail_async();
                async_sending.execute();
            }
        });

    }

    private class sender_mail_async extends AsyncTask<Object, String, Boolean> {
        ProgressDialog WaitingDialog;

        @Override
        protected void onPreExecute() {
            WaitingDialog = ProgressDialog.show(ExtendedMailOrg.this, "Выполняется", "Пожалуйста подождите", true);
        }

        @Override
        protected void onPostExecute(Boolean result) {
            WaitingDialog.dismiss();
            Toast.makeText(mainContext, "Ваши пожелания учтены,спасибо!", Toast.LENGTH_LONG).show();
            ((Activity) mainContext).finish();
        }


        @Override
        protected Boolean doInBackground(Object... params) {

            try {
                title1 = ((TextInputLayout) findViewById(R.id.txt1));
                title = ((EditText) title1.findViewById(R.id.first_name)).getText().toString();
                name1 = ((TextInputLayout) findViewById(R.id.txt2));
                name = ((EditText) name1.findViewById(R.id.name)).getText().toString();
                text1 = ((TextInputLayout) findViewById(R.id.txt3));
                text = ((EditText) text1.findViewById(R.id.about)).getText().toString();
                all_text = ("Контактные данные: " + name + "\n" + "Отзыв|Предложение: " + text);


                from = "feedbackmail@gmail.ru";
                where = "Timur.Leroy@yandex.ru";

                MailSenderClass sender = new MailSenderClass("Feedmailback@gmail.com", "654321ytrewq");

                sender.sendMail(title, all_text, from, where, attach);
            } catch (Exception e) {
                Toast.makeText(mainContext, "упс!", Toast.LENGTH_SHORT).show();
            }

            return false;
        }


    }
}