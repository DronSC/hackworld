package com.kolibru.hackworld.Fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.florent37.materialviewpager.MaterialViewPagerHelper;
import com.github.florent37.materialviewpager.adapter.RecyclerViewMaterialAdapter;
import com.kolibru.hackworld.Item.NewsItem;
import com.kolibru.hackworld.ModelViews.NewsViewModel;
import com.kolibru.hackworld.R;
import com.kolibru.hackworld.databinding.FragmentMainBinding;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ashotik05 on 10.12.2015.
 */
public class SecondFragment extends Fragment {
    List<NewsItem> data = new ArrayList<>();
    private NewsViewModel NVM;
    private FragmentMainBinding mBinding;

    private static final String FILTER_TIMER = "filter_timer";


    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View fragment_view = inflater.inflate(R.layout.fragment_main, container, false);
        return fragment_view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        data = getDatalist();

        NVM = new NewsViewModel();//создаем модель работы с содержимым RecycleView
        for (NewsItem ni : data)
            NVM.addNews(ni);
        mBinding = DataBindingUtil.bind(getView());//получаем структуру XML фрагмента - куда будет помещена модель

        mBinding.mainList.setLayoutManager(new LinearLayoutManager(getActivity()));
        mBinding.mainList.setAdapter(new RecyclerViewMaterialAdapter(NVM.NA));
//        NVM.setOnNewsClickListener(new NewsViewModel.OnNewsClickListener() {//задаем обработчик нажатия
//            @Override
//            public void onNewsClick(NewsItem mNews) {
//                Intent intent = new Intent(getActivity(), NewsDetailActivity.class);
//                intent.putExtra(NewsItem.class.getCanonicalName(), mNews);
//                startActivity(intent);
//            }
//        });
        mBinding.setNewsViewModel(NVM);
        MaterialViewPagerHelper.registerRecyclerView(getActivity(), mBinding.mainList, null);
    }


    private List<NewsItem> getDatalist() {
        List<NewsItem> data = new ArrayList<>();
        data.add(new NewsItem("Зарядка", "", "09:00-09-30").withImage(R.drawable.ic_ghost).withSubhead("Не помешает нам всем размяться,чтобы лучше думалось"));
        data.add(new NewsItem("Завтрак", "", "09:30-10:00").withImage(R.drawable.ic_morning).withSubhead("Хороший завтрак-заряд на весь день! "));
        data.add(new NewsItem("Чек поинт для руководителей", "", "10:00-10:30").withImage(R.drawable.ic_tie));
        data.add(new NewsItem("Работа над проектом", "", " 10:30-12:00").withImage(R.drawable.ic_profile).withSubhead("Каждый участник мероприятия останется довольным"));
        data.add(new NewsItem("Мастер класс", "", "12:00-13:00").withImage(R.drawable.guy2).withSubhead("Кир Ященко"));
        data.add(new NewsItem("Обед", "", "13:00-13:30").withImage(R.drawable.ic_food).withSubhead("Набираемся сил для дальнейшей работы"));
        data.add(new NewsItem("Работа над проектом ", "", "13:30-16:00").withImage(R.drawable.ic_work).withSubhead("У вас обязательно получится"));
        data.add(new NewsItem("Неформальное общение команд", "", " 16:00-17:00").withImage(R.drawable.ic_chat).withSubhead("Обмениваемся опытом,болтаем"));
        data.add(new NewsItem("Мастер класс", "", "17:00-18:00").withImage(R.drawable.guy1).withSubhead("Николай Сусолкин"));
        data.add(new NewsItem("Работа над проектом", "", "18:00-20:00").withImage(R.drawable.ic_code).withSubhead("Начинаем кодить и идти к победе"));
        return data;
    }

}
