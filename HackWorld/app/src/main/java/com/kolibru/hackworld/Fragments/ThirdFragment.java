package com.kolibru.hackworld.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.github.florent37.materialviewpager.MaterialViewPagerHelper;
import com.github.florent37.materialviewpager.adapter.RecyclerViewMaterialAdapter;
import com.kolibru.hackworld.Item.NewsItem;
import com.kolibru.hackworld.ModelViews.NewsViewModel;

import java.util.ArrayList;
import java.util.List;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kolibru.hackworld.R;
import com.kolibru.hackworld.databinding.FragmentMainBinding;

/**
 * Created by Ashotik05 on 11.12.2015.
 */
public class ThirdFragment extends Fragment {
        List<NewsItem> data = new ArrayList<>();
        private NewsViewModel NVM;
        private FragmentMainBinding mBinding;

        private static final String FILTER_TIMER = "filter_timer";


        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                                 @Nullable Bundle savedInstanceState) {
            View fragment_view = inflater.inflate(R.layout.fragment_main, container, false);
            return fragment_view;
        }

        @Override
        public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
            data = getDatalist();

            NVM = new NewsViewModel();//создаем модель работы с содержимым RecycleView
            for (NewsItem ni : data)
                NVM.addNews(ni);
            mBinding = DataBindingUtil.bind(getView());//получаем структуру XML фрагмента - куда будет помещена модель

            mBinding.mainList.setLayoutManager(new LinearLayoutManager(getActivity()));
            mBinding.mainList.setAdapter(new RecyclerViewMaterialAdapter(NVM.NA));
//        NVM.setOnNewsClickListener(new NewsViewModel.OnNewsClickListener() {//задаем обработчик нажатия
//            @Override
//            public void onNewsClick(NewsItem mNews) {
//                Intent intent = new Intent(getActivity(), NewsDetailActivity.class);
//                intent.putExtra(NewsItem.class.getCanonicalName(), mNews);
//                startActivity(intent);
//            }
//        });
            mBinding.setNewsViewModel(NVM);
            MaterialViewPagerHelper.registerRecyclerView(getActivity(), mBinding.mainList, null);
        }


        private List<NewsItem> getDatalist() {
            List<NewsItem> data = new ArrayList<>();
            data.add(new NewsItem("Зарядка", "", "09:00-09-30").withImage(R.drawable.ic_ghost).withSubhead("Последняя зарядка на Хакатоне"));
            data.add(new NewsItem("Завтрак", "", "09:30-10:00").withImage(R.drawable.ic_morning).withSubhead("Хороший завтрак-заряд на весь день"));
            data.add(new NewsItem("Чек поинт для руководителей", "", "10:00-11:00").withImage(R.drawable.ic_tie));
            data.add(new NewsItem("Мастер класс", "", " 11:00-12:00").withImage(R.drawable.guy3).withSubhead("Трофим Жугастров"));
            data.add(new NewsItem("Работа над проектами", "", "12:00-15:00").withImage(R.drawable.ic_work).withSubhead("Подготовка финальной презентации"));
            data.add(new NewsItem("Знакомство жюри с проектами", "", "15:00-16:00").withImage(R.drawable.ic_eye).withSubhead(" "));
            data.add(new NewsItem("Презентация проектов", "", "16:00-18:00").withImage(R.drawable.ic_star).withSubhead("Демонстрация прототипов"));
            data.add(new NewsItem("Неформальное общение участников   ", "", "18:00-18:30").withImage(R.drawable.ic_chat).withSubhead("Каждому есть о чем рассказать"));
            data.add(new NewsItem("Объявление победителей", "", "18:30-19:00").withImage(R.drawable.ic_finish).withSubhead("Это будет тяжело,но победит достойный"));
            return data;
        }
    }

