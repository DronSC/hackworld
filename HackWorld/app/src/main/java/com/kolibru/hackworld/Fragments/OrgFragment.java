package com.kolibru.hackworld.Fragments;


import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.florent37.materialviewpager.MaterialViewPagerHelper;
import com.github.florent37.materialviewpager.adapter.RecyclerViewMaterialAdapter;
import com.kolibru.hackworld.Item.NewsItem;
import com.kolibru.hackworld.Item.OrgItem;
import com.kolibru.hackworld.ModelViews.NewsViewModel;
import com.kolibru.hackworld.ModelViews.OrgViewModel;
import com.kolibru.hackworld.R;
import com.kolibru.hackworld.databinding.FragmentAboutBinding;
import com.kolibru.hackworld.databinding.FragmentOrgBinding;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class OrgFragment extends Fragment {

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_org, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        FragmentOrgBinding mBinding = DataBindingUtil.bind(getView());
        OrgViewModel NVM = new OrgViewModel();//создаем модель работы с содержимым RecycleView
        List<OrgItem> data =  getDatalist();
        for (OrgItem ni : data)
            NVM.addNews(ni);
        mBinding = DataBindingUtil.bind(getView());//получаем структуру XML фрагмента - куда будет помещена модель
        //mBinding.recyclerView.setHasFixedSize(true);

        mBinding.recyclerView.setLayoutManager(new GridLayoutManager(getActivity(),2));
        mBinding.recyclerView.setAdapter(NVM.NA);

    }

    private List<OrgItem> getDatalist() {
        List<OrgItem> data = new ArrayList<>();
        data.add(new OrgItem("Анна Павловская").withImage(R.drawable.im0).withSubhead("Главный организатор мероприятия"));
        data.add(new OrgItem("Дима Волошин").withImage(R.drawable.im1).withSubhead("Главный веб-разработчик"));
        data.add(new OrgItem("Александр Боровлев").withImage(R.drawable.im8).withSubhead("Свециалист по организации и проведению мероприятий"));
        data.add(new OrgItem("Александр Швидченко").withImage(R.drawable.im2).withSubhead("Специалист по связям с общественностью"));
        data.add(new OrgItem("Андрей Малыгин").withImage(R.drawable.im3).withSubhead("Специалист по IT-разработке и орагнизации праздника"));
        data.add(new OrgItem("Кристина Костенко").withImage(R.drawable.im4).withSubhead("Координатор и руководитель рабочей группы"));
        data.add(new OrgItem("Юрий Хабовец").withImage(R.drawable.im5).withSubhead("Организатор рабочей обстановки"));
        data.add(new OrgItem("Маргарита Давыдюк").withImage(R.drawable.im6).withSubhead("Организатор сытого настроения"));
        data.add(new OrgItem("Ваня Воронченко").withImage(R.drawable.im7).withSubhead("Связи с рабочими группами"));
        data.add(new OrgItem("А так же...").withImage(R.drawable.im99).withSubhead("многие многие другие"));
        return data;
    }
}
