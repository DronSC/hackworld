package com.kolibru.hackworld.Fragments;


import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.florent37.materialviewpager.adapter.RecyclerViewMaterialAdapter;
import com.kolibru.hackworld.Item.OrgItem;
import com.kolibru.hackworld.ModelViews.OrgViewModel;
import com.kolibru.hackworld.ModelViews.SponViewModel;
import com.kolibru.hackworld.R;
import com.kolibru.hackworld.databinding.FragmentAboutBinding;
import com.kolibru.hackworld.databinding.FragmentOrgBinding;
import com.kolibru.hackworld.databinding.FragmentSponBinding;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class SponFragment extends Fragment {

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_org, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        FragmentOrgBinding mBinding = DataBindingUtil.bind(getView());
        SponViewModel NVM = new SponViewModel();//создаем модель работы с содержимым RecycleView
        List<OrgItem> data =  getDatalist();
        for (OrgItem ni : data)
            NVM.addNews(ni);
        mBinding = DataBindingUtil.bind(getView());//получаем структуру XML фрагмента - куда будет помещена модель
        mBinding.recyclerView.setHasFixedSize(true);

        mBinding.recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        mBinding.recyclerView.setAdapter(NVM.NA);

    }

    private List<OrgItem> getDatalist() {
        List<OrgItem> data = new ArrayList<>();
        data.add(new OrgItem("ДГТУ").withImage(R.drawable.log0));
        data.add(new OrgItem("Дима Волошин").withImage(R.drawable.log1));
        data.add(new OrgItem("Александр Боровлев").withImage(R.drawable.log2));
        data.add(new OrgItem("Александр Швидченко").withImage(R.drawable.log3));
        data.add(new OrgItem("Андрей Малыгин").withImage(R.drawable.log4));
        data.add(new OrgItem("Юрий Хабовец").withImage(R.drawable.log6));
        data.add(new OrgItem("Маргарита Давыдюк").withImage(R.drawable.log8));
        data.add(new OrgItem("Ваня Воронченко").withImage(R.drawable.log9));
        data.add(new OrgItem("А так же...").withImage(R.drawable.log10));
        data.add(new OrgItem("Ваня Воронченко").withImage(R.drawable.log11));
        data.add(new OrgItem("Ваня Воронченко").withImage(R.drawable.log12));
        data.add(new OrgItem("Ваня Воронченко").withImage(R.drawable.log13));
        data.add(new OrgItem("Ваня Воронченко").withImage(R.drawable.log14));
        return data;
    }

}
