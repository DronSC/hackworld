package com.kolibru.hackworld.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.kolibru.hackworld.Item.NewsItem;
import com.kolibru.hackworld.R;

public class NewsDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_detail);

        try {
            NewsItem mNews = getIntent().getParcelableExtra(NewsItem.class.getCanonicalName());
            Toast.makeText(this, "Ой, как удачно нажал по новости с текстом: " + mNews.title.get(), Toast.LENGTH_LONG).show();


        } catch (Exception e) {
            Toast.makeText(this, " А вот не получил я новость!", Toast.LENGTH_LONG).show();
        }
    }
}
