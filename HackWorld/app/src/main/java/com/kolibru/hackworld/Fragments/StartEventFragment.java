package com.kolibru.hackworld.Fragments;


import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kolibru.hackworld.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.text.SimpleDateFormat;
import java.util.Date;

@EFragment(R.layout.fragment_start_event)
public class StartEventFragment extends Fragment {

    @ViewById
    TextView txtTimerDay, txtTimerHour, txtTimerMinute, txtTimerSecond, txt_TimerDay, txt_TimerHour, txt_TimerMinute, txt_TimerSecond;


    private Handler handler;

    private Runnable runnable;


    @AfterViews
    void ready() {
        countDownStart();
    }

    public void countDownStart() {
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                handler.postDelayed(this, 1000);
                try {
                    SimpleDateFormat dateFormat = new SimpleDateFormat(
                            "yyyy-MM-dd");
                    Date futureDate = dateFormat.parse("2015-12-25");
                    Date currentDate = new Date();
                    if (!currentDate.after(futureDate)) {
                        long diff = futureDate.getTime()
                                - currentDate.getTime();
                        long days = diff / (24 * 60 * 60 * 1000);
                        diff -= days * (24 * 60 * 60 * 1000);
                        long hours = diff / (60 * 60 * 1000);
                        diff -= hours * (60 * 60 * 1000);
                        long minutes = diff / (60 * 1000);
                        diff -= minutes * (60 * 1000);
                        long seconds = diff / 1000;
                        txtTimerDay.setText("" + String.format("%02d", days));
                        txtTimerHour.setText("" + String.format("%02d", hours));
                        txtTimerMinute.setText(""
                                + String.format("%02d", minutes));
                        txtTimerSecond.setText(""
                                + String.format("%02d", seconds));
                    } 
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        handler.postDelayed(runnable, 1 * 1000);
    }

    public void textViewGone() {
        txtTimerDay.setVisibility(View.GONE);
        txt_TimerDay.setVisibility(View.GONE);
        txtTimerHour.setVisibility(View.GONE);
        txt_TimerHour.setVisibility(View.GONE);
        txtTimerMinute.setVisibility(View.GONE);
        txt_TimerMinute.setVisibility(View.GONE);
        txtTimerSecond.setVisibility(View.GONE);
        txt_TimerSecond.setVisibility(View.GONE);
    }

}
