package com.kolibru.hackworld.Fragments;


import android.content.pm.PackageInfo;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kolibru.hackworld.R;
import com.kolibru.hackworld.databinding.FragmentAboutBinding;

import java.util.Calendar;


public class AboutFragment extends Fragment {

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_about, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        FragmentAboutBinding mBinding = DataBindingUtil.bind(getView());

        Calendar d = Calendar.getInstance();

        try {
            if (d.get(Calendar.YEAR) == 2015)
                mBinding.date.setText("© " + Integer.toString(d.get(Calendar.YEAR)) + " © All rights reserved.");
            else
                mBinding.date.setText("© 2015-" + Integer.toString(d.get(Calendar.YEAR)) + " © All rights reserved.");

            mBinding.version.setText("Версия " + String.valueOf(Build.VERSION.CODENAME));
        } catch (Exception e) {
        }

    }


}
