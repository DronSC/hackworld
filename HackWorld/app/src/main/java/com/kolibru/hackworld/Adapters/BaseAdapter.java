package com.kolibru.hackworld.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.kolibru.hackworld.R;

import java.util.List;


public abstract class BaseAdapter<T, VH extends BaseAdapter.ViewHolder> extends RecyclerView.Adapter<VH> implements View.OnClickListener {

    List<T> source;
    OnItemClickListener mOnItemClickListener;//храним обработчик нажатий

    public BaseAdapter(List<T> source) {
        this.source = source;
    }

    @Override
    public void onBindViewHolder(VH holder, int position) {
        holder.itemView.setTag(R.id.position, position);
    }

    @Override
    public final VH onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = onCreateView(parent, viewType);
        itemView.setOnClickListener(this);
        return onCreateViewHolder(itemView);
    }

    public abstract View onCreateView(ViewGroup parent, int viewType);

    public abstract VH onCreateViewHolder(View itemView);

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.mOnItemClickListener = onItemClickListener;
    }

    @Override
    public int getItemCount() {
        return source.size();
    }


    @Override
    public void onClick(View v) {       //когда нажали объект адаптера
        int position = (int) v.getTag(R.id.position);
        if (mOnItemClickListener != null) { //если есть обработчик нажатия - то запускаем его
            mOnItemClickListener.onItemClick(source.get(position), position);
        }
    }

    public interface OnItemClickListener<T> {//интерфейс работы нажатия

        void onItemClick(T item, int position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        View itemView;

        public ViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
        }
    }
}
