package com.kolibru.hackworld.Activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.kolibru.hackworld.Feedback.ExtendedMail;
import com.kolibru.hackworld.Feedback.ExtendedMailOrg;
import com.kolibru.hackworld.Fragments.AboutFragment;
import com.kolibru.hackworld.Fragments.DialogFragmentDeveloper;
import com.kolibru.hackworld.Fragments.DialogFragmentOrg;
import com.kolibru.hackworld.Fragments.MapFragment_;
import com.kolibru.hackworld.Fragments.OrgFragment;
import com.kolibru.hackworld.Fragments.SponFragment;
import com.kolibru.hackworld.Fragments.StartEventFragment_;
import com.kolibru.hackworld.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity {

    @ViewById(R.id.drawer_layout)
    DrawerLayout mDrawer;

    private ActionBarDrawerToggle mDrawerToggle;

    @ViewById(R.id.toolbar)
    Toolbar toolbar;

    @ViewById(R.id.nvView)
    NavigationView nvView;

    MenuItem selectItem = null;




    @AfterViews
    void ready() {
        checkShowTutorial();
        if (toolbar != null) {
            toolbar.setTitle("Хакатон в ДГТУ");
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        nvView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                mDrawer.closeDrawers();

                if (selectItem != menuItem) {
                    selectItem = menuItem;

                    switch (menuItem.getItemId()) {
                        case R.id.main:
                            toolbar.setTitle("Хакатон в ДГТУ");
                            changeFragment(new StartEventFragment_.FragmentBuilder_().build());
                            break;
                        case R.id.tutorial:
                            Intent intent = new Intent(MainActivity.this, ProductTourActivity.class);
                            startActivity(intent);
                            break;
                        case R.id.task:
                            intent = new Intent(MainActivity.this, PlanActivity_.class);
                            startActivity(intent);
                            break;
                        case R.id.contact:
                            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                            builder.setTitle("С кем желаете связаться?")
                                    .setCancelable(true)
                                    .setPositiveButton("Разработчиком",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    DialogFragment dialogFragment=new DialogFragmentDeveloper();

                                                    dialogFragment.show(getSupportFragmentManager(), "developer");
                                                }
                                            })
                                    .setNeutralButton("Организатором",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    DialogFragment dialogFragment=new DialogFragmentOrg();
                                                    dialogFragment.show(getSupportFragmentManager(), "org");
                                                }
                                            });
                            AlertDialog alert = builder.create();
                            alert.show();
                            break;
                        case R.id.org:
                            toolbar.setTitle("Организаторы хакатона");
                            changeFragment(new OrgFragment());
                            break;
                        case R.id.spon:
                            toolbar.setTitle("Спонсоры мероприятия");
                            changeFragment(new SponFragment());
                            break;
                        case R.id.map:
                            //changeFragment(new MapFragment_.FragmentBuilder_().build());
                            Toast.makeText(MainActivity.this,"Мы как раз занимаемся украшением помещения, как все будет готовы - Вы увидите!",Toast.LENGTH_LONG).show();
                            break;
                        case R.id.about:
                            toolbar.setTitle("О приложении");
                            changeFragment(new AboutFragment());
                            break;
                    }
                }

                return true;
            }
        });


        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawer, R.string.app_name, R.string.app_name) {

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {

                super.onDrawerOpened(drawerView);
            }
        };

        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        mDrawer.setDrawerListener(mDrawerToggle);

        mDrawerToggle.syncState();

        getSupportFragmentManager().beginTransaction().add(R.id.fragment, new StartEventFragment_.FragmentBuilder_().build()).commit();
    }

    private void changeFragment(Fragment frag) {
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment, frag).commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {

            case android.R.id.home:
                mDrawer.openDrawer(GravityCompat.START);
                return true;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (mDrawer != null)
            mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (mDrawer != null)
            mDrawerToggle.onConfigurationChanged(newConfig);
    }

    private void checkShowTutorial() {
        //int oldVersionCode = PrefConstants.getAppPrefInt(this, "version_code");
        //  int currentVersionCode = SAppUtil.getAppVersionCode(this);
        // if(currentVersionCode>oldVersionCode){
        startActivity(new Intent(MainActivity.this, ProductTourActivity.class));
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        //  PrefConstants.putAppPrefInt(this, "version_code", currentVersionCode);
        //  }
    }


}
